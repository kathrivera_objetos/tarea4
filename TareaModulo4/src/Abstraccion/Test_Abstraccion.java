package Abstraccion;
public class Test_Abstraccion {
    public static void main(String[] args) {
        
        Pais[] pais = new Pais[3];
        pais[0] = new Honduras();
        pais[1] = new Guatemala();
        pais[2] = new El_Salvador();
 
        for(int x=0;x<pais.length;x++){
            System.out.println(
                    "Pais: "+
            pais[x].getPais() + "\nPresidente: "+
                            pais[x].getPresidente() +"\n"
            );
        }
     }
    
}
