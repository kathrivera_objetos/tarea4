package Encapsulamiento;
public class Ciudadano {
private String nombre;
private String apellido;
private int edad;
private String exp;

    public Ciudadano() {
    }

    public Ciudadano(String nombre, String apellido, int edad, String exp) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.exp = exp;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }
    public void imprimir(){
        System.out.println(
        "Nombre: "+this.getNombre()+" "+this.getApellido()+
                "\nEdad: "+this.getEdad()+
                "\nExperiencia: "+this.getExp()+"\n"
        );
    }
}
