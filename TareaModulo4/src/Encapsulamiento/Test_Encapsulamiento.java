package Encapsulamiento;
public class Test_Encapsulamiento {
    public static void main(String[] args) {
        Ciudadano ciud = new Ciudadano("Katherin","Rivera",22,"8meses laborando en una empresa X");
        Ciudadano ciud2=new Ciudadano();
        
        ciud2.setNombre("Alejandro");
        ciud2.setApellido("Tomillo");
        ciud2.setEdad(45);
        ciud2.setExp("Ninguna");
        
        System.out.println("Ciudadano #1");
        ciud.imprimir();
        
                System.out.println("Ciudadano #2");
        ciud2.imprimir();
    }
    
}
